# SPSP Portal

## Description
The SPSP Portal handles the requests from the frontend (SPSP User Interface), verifies the authorizations on keycloak and bridges to the backend (SPSP Loader and SPSP Server).

## Authors
Main developer: Daniel Walther

## License
The source code is licensed under GPL-3.0-or-later and available via GitLab.
For more information or any inquiries, please reach out to legal@sib.swiss.