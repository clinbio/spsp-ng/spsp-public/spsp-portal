package swiss.sib.clinbio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpspPortalApplication {

    public static void main( String[] args ) {
        SpringApplication.run( SpspPortalApplication.class, args );
    }
}
