package swiss.sib.clinbio.spspportal.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import swiss.sib.clinbio.spspportal.keycloack.AccessToken;
import swiss.sib.clinbio.spspportal.keycloack.KeycloakJwtToken;
import swiss.sib.clinbio.spspportal.tools.SpspProperties;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.util.Map;

@RestController
@RequestMapping
@Slf4j
public class HomeController {
    @Autowired
    AccessToken accessToken;

    @Autowired
    SpspProperties spspProperties;

    @GetMapping( "/isSpspRunning" )
    public ResponseEntity<String> isSpspRunning() {
        return ResponseEntity.ok( spspProperties.isSpspRunning() ? "ok" : "" );
    }

    @GetMapping( "/isSpspWorking" )
    public ResponseEntity<String> isSpspWorking() {
        return ResponseEntity.ok( spspProperties.isSpspWorking() ? "ok" : "" );
    }

    @GetMapping( value = { "/", "/home" } )
    public ModelAndView home() {
       return new ModelAndView( "Home.html" );
    }

    @PostMapping( "/mail" )
    public ResponseEntity<String> sendMail(
            @RequestHeader HttpHeaders header,
            @RequestBody Map<String, String> params ) {

        String url = SpspProperties.get().getServerUrl() + "/mail";
        try {
            AccessToken.UserDto user = accessToken.getUser( header );
            String sender = user.laboratory + " - " + user.userName + " (" + user.email + ")";
            params.put( "sender", sender );
            return Tools.restTemplate().postForEntity( url, params, String.class );

        } catch( KeycloakJwtToken.InvalidTokenException ex ) {
            log.error( url + ": " + ex.getMessage() );
            return ResponseEntity.status( HttpStatus.FORBIDDEN ).build();
        }
    }

    @GetMapping( "/news" )
    public ResponseEntity<String> getNews() {
        return Tools.restTemplate().getForEntity( SpspProperties.get().getServerUrl() + "/news", String.class );
    }
}
