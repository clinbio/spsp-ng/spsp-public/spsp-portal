package swiss.sib.clinbio.spspportal.controller;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import swiss.sib.clinbio.spspportal.keycloack.AccessToken;
import swiss.sib.clinbio.spspportal.keycloack.KeycloakJwtToken;
import swiss.sib.clinbio.spspportal.tools.SpspProperties;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

@RestController
@RequestMapping
@Slf4j
public class QueryController {
    @Autowired
    SpspProperties spspProperties;

    @Autowired
    AccessToken accessToken;

    @GetMapping( "/portal_query_vocabulary" )
    public ResponseEntity<String> getPortalQueryVocabulary() {
        return Tools.restTemplate().getForEntity( getServerUrl( "/portal_query_vocabulary" ), String.class );
    }

    @PostMapping( QueryDto.queryApi )
    public ResponseEntity<String> portalQuery(
            @RequestHeader HttpHeaders header,
            @RequestBody QueryDto dto ) {

        return apiCallWithAuthorization( QueryDto.queryApi, () -> {
            accessToken.getLaboratoryAndRoles( header, dto );
            log.info( getJson( dto ) );
            ResponseEntity<String> response = Tools.restTemplate().postForEntity( getServerUrl( QueryDto.queryApi ), dto, String.class );
            if( response.getStatusCode() != HttpStatus.OK )
                log.error( response.getBody() );
            return response;

        } );
    }

    @PostMapping( QueryDto.downloadApi )
    public ResponseEntity<String> portalDownloadQuery(
            @RequestHeader HttpHeaders header,
            @RequestBody QueryDto dto ) {

        return apiCallWithAuthorization( QueryDto.queryApi, () -> {
            accessToken.getLaboratoryAndRoles( header, dto );
            log.info( "download " + dto.criteria );
            ResponseEntity<String> response = Tools.restTemplate().postForEntity( getServerUrl( QueryDto.downloadApi ), dto, String.class );
            if( response.getStatusCode() != HttpStatus.OK )
                log.error( response.getBody() );
            return response;

        } );
    }

    public static class QueryDto {
        static final String queryApi = "/portal_query";
        static final String downloadApi = "/portal_download";

        public String columns;
        public String criteria;
        public String sortOrder;

        public int firstIndex;
        public int lastIndex;

        public Date timeStamp;
        public String userName;
        public String laboratory;
        public List<String> roles;
    }

    //=== filter ===

    static class FilterId {
        @NotBlank
        public String name;

        public String groupKey;
    }

    public static class FilterDto extends FilterId {
        static final String cApi = "/query_save";

        public String description;
        public String columns;
        @NotBlank
        public String criteria;
        public String sortOrder;
    }

    static final String getFilterApi = "/query_filter";

    @GetMapping( getFilterApi )
    public ResponseEntity<String> getFilter( @RequestHeader HttpHeaders header ) {

        return apiCallWithAuthorization( getFilterApi, () -> {
            String url = getServerUrl( getFilterApi ) + "?group=" + accessToken.getFilterOwner( header );
            log.info( url );
            return Tools.restTemplate().getForEntity( url, String.class );

        } );
    }

    @PostMapping( FilterDto.cApi )
    public ResponseEntity<String> storeFilter(
            @RequestHeader HttpHeaders header,
            @Valid @RequestBody FilterDto dto, BindingResult validationResult ) {

        return apiCallWithAuthorization( FilterDto.cApi, () -> {
            dto.groupKey = accessToken.getFilterOwnerForEditing( header );
            log.info( "Store filter: " + getJson( dto ) );
            return Tools.restTemplate().postForEntity( getServerUrl( FilterDto.cApi ), dto, String.class );
        } );
    }

    public static class DeleteDto extends FilterId {
        static final String cApi = "/query_delete";
    }

    @PostMapping( DeleteDto.cApi )
    public ResponseEntity<String> deleteFilter(
            @RequestHeader HttpHeaders header,
            @Valid @RequestBody DeleteDto dto, BindingResult validationResult ) {

        return apiCallWithAuthorization( DeleteDto.cApi, () -> {
            dto.groupKey = accessToken.getFilterOwnerForEditing( header );
            log.info( "Delete filter: " + getJson( dto ) );
            return Tools.restTemplate().postForEntity( getServerUrl( DeleteDto.cApi ), dto, String.class );
        } );
    }

    public static class RenameDto extends FilterId {
        static final String cApi = "/query_rename";

        @NotBlank
        public String newName;
    }

    @PostMapping( RenameDto.cApi )
    public ResponseEntity<String> renameFilter(
            @RequestHeader HttpHeaders header,
            @Valid @RequestBody RenameDto dto, BindingResult validationResult ) {

        return apiCallWithAuthorization( RenameDto.cApi, () -> {
            dto.groupKey = accessToken.getFilterOwnerForEditing( header );
            log.info( "Rename filter: " + getJson( dto ) );
            return Tools.restTemplate().postForEntity( getServerUrl( RenameDto.cApi ), dto, String.class );
        } );
    }

    static final String laboratoryReportApi = "/loader_report";

    @GetMapping( laboratoryReportApi )
    public ResponseEntity<String> getLoaderSubmissionReport(
            @RequestHeader HttpHeaders header,
            @RequestParam Map<String, String> params ) {

        return apiCallWithAuthorization( laboratoryReportApi, () -> {
            String url = spspProperties.getLoaderUrl() + laboratoryReportApi +
                    "?days=" + params.get( "days" ) + "&lab=" + accessToken.getFilterOwner( header );
            log.info( url );
            return Tools.restTemplate().getForEntity( url, String.class );
        } );
    }

    interface ApiCall {
        ResponseEntity<String> call() throws KeycloakJwtToken.InvalidTokenException, RestClientException;
    }

    private ResponseEntity<String> apiCallWithAuthorization( String apiName, ApiCall apiFct ) throws RestClientException {
        try {
            return apiFct.call();

        } catch( KeycloakJwtToken.InvalidTokenException ex ) {
            log.error( apiName + ": " + ex.getMessage() );
            return ResponseEntity.status( HttpStatus.FORBIDDEN ).build();

        } catch( RestClientException ex2 ) {
            log.error( apiName + ": " + ex2.getMessage() );
            return ResponseEntity.status( HttpStatus.BAD_REQUEST ).build();
        }
    }

    @GetMapping( "/gender_stat" )
    public ResponseEntity<String> getGenderStat() {
        return Tools.restTemplate().getForEntity( getServerUrl( "/gender_stat" ), String.class );
    }

    @GetMapping( "/age_stat" )
    public ResponseEntity<String> getAgeStat() {
        return Tools.restTemplate().getForEntity( getServerUrl( "/age_stat" ), String.class );
    }

    @GetMapping( "/lineage_stat" )
    public ResponseEntity<String> getLoaderSubmissionReport( @RequestParam Map<String, Object> params ) {
        String url = getServerUrl( "/lineage_stat" );
        if( !params.isEmpty() ) {
            StringJoiner joiner = new StringJoiner( "&", url + "?", "" );
            for( var entry : params.entrySet() )
                joiner.add( entry.getKey() + "=" + entry.getValue() );
            url = joiner.toString();
        }
        return Tools.restTemplate().getForEntity( url, String.class );
    }

    private String getServerUrl( String apiName ) {
        return spspProperties.getServerUrl() + apiName;
    }

    private String getJson( Object o ) {
        return Tools.exceptionCatcher( () -> Tools.getJson( o ), "JSON fail" );
    }
}
