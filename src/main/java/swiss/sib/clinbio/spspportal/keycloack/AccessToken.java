package swiss.sib.clinbio.spspportal.keycloack;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import swiss.sib.clinbio.spspportal.controller.QueryController;
import swiss.sib.clinbio.spspportal.tools.SpspProperties;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringJoiner;

@Component
@Slf4j
public class AccessToken {
    private static final String RESOURCE_NAME = "SPSP-app";
    private static final String SPSP_EDITOR = "SPSP Editor";
    private static final String SPSP_VIEWER = "SPSP Viewer";
    private static final String SPSP_ADMIN = "SPSP Admin";

    static final String CLAIM_NAME = "name";
    private static final String CLAIM_EMAIL = "email";

    private final KeycloakJwtToken token;

    AccessToken( SpspProperties properties ) {
        token = new KeycloakJwtToken( properties.getUrlKeycloak() );
    }

    public void getLaboratoryAndRoles( HttpHeaders headers, QueryController.QueryDto dto ) throws KeycloakJwtToken.InvalidTokenException {
        DecodedJWT decodedJWT = token.decodeToken( headers );
        UserDto userDto = getUser( decodedJWT );
        dto.roles = token.getRoles( RESOURCE_NAME, decodedJWT );
        for( int i = 0; i < dto.roles.size(); i++ ) {
            String role = dto.roles.get( i );
            if( role.startsWith( "SPSP " ) ) {
                dto.roles.remove( i );
                break;
            }
        }
        if( userDto.laboratory == null )
            throw new KeycloakJwtToken.InvalidTokenException( "No membership defined" );
        dto.laboratory = userDto.laboratory;
        dto.userName = userDto.userName;
        dto.timeStamp = new Date();

        log.info( "Token: " + getInformation( decodedJWT ) );
    }

    public UserDto getUser( HttpHeaders headers ) throws KeycloakJwtToken.InvalidTokenException {
        return getUser( token.decodeToken( headers ) );
    }

    UserDto getUser( DecodedJWT decodedJWT ) throws KeycloakJwtToken.InvalidTokenException {
        UserDto userDto = new UserDto();
        List<String> groups = token.getGroups( decodedJWT );
        for( String role : groups ) {
            role = role.toUpperCase( Locale.ROOT );
            if( role.startsWith( "LAB_" ) ) {
                if( userDto.laboratory != null )
                    throw new KeycloakJwtToken.InvalidTokenException( "Multiple membership defined" );

                userDto.laboratory = role;
            } else if( role.equalsIgnoreCase( SPSP_EDITOR ) ) {
                userDto.isEditor = true;
            }
        }

        userDto.userName = decodedJWT.getClaim( CLAIM_NAME ).asString();
        userDto.email = decodedJWT.getClaim( CLAIM_EMAIL ).asString();
        return userDto;
    }

    public String getFilterOwner( HttpHeaders headers ) throws KeycloakJwtToken.InvalidTokenException {
        UserDto userDto = getUser( headers );
        if( userDto.laboratory == null )
            throw new KeycloakJwtToken.InvalidTokenException( "No membership defined" );
        return userDto.laboratory;
    }

    public String getFilterOwnerForEditing( HttpHeaders headers ) throws KeycloakJwtToken.InvalidTokenException {
        AccessToken.UserDto userDto = getUser( headers );
        if( !userDto.isEditor )
            throw new KeycloakJwtToken.InvalidTokenException( "Need editor rights" );
        return userDto.laboratory;
    }

    public static class UserDto {
        public String userName = null;
        public String email = null;
        public String laboratory = null;
        public boolean isEditor = false;

        public void add( StringJoiner joiner ) {
            joiner.add( laboratory )
                    .add( "" + isEditor )
                    .add( userName )
                    .add( email );
        }
    }

    public String getInformation( HttpHeaders headers ) {
        try {
            return getInformation( token.decodeToken( headers ) );
        } catch( KeycloakJwtToken.InvalidTokenException ex ) {
            return ex.getMessage();
        }
    }

    public String getInformation( DecodedJWT decodedJWT ) throws KeycloakJwtToken.InvalidTokenException {
        UserDto userDto = getUser( decodedJWT );
        List<String> rolesList = token.getRoles( RESOURCE_NAME, decodedJWT );

        StringJoiner joiner = new StringJoiner( ", " );
        userDto.add( joiner );
        StringJoiner roles = new StringJoiner( ", ", "{ ", "}" );
        for( String role : rolesList )
            roles.add( role );
        joiner.add( roles.toString() );
        return joiner.toString();
    }
}
