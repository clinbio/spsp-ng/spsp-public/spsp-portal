package swiss.sib.clinbio.spspportal.keycloack;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.SigningKeyNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class KeycloakJwkProvider implements JwkProvider {

    private final URI uri;

    public KeycloakJwkProvider( String jwkProviderUrl ) {
        try {
            this.uri = new URI( jwkProviderUrl ).normalize();
        } catch( URISyntaxException e ) {
            throw new IllegalArgumentException( "Invalid jwks uri", e );
        }
    }

    @Override
    public Jwk get( String keyId ) throws JwkException {
        final List<Jwk> jwks = getAll();
        if( keyId == null ) {
            if( jwks.size() == 1 )
                return jwks.get( 0 );
        } else {
            for( Jwk jwk : jwks )
                if( keyId.equals( jwk.getId() ) )
                    return jwk;
        }

        throw new SigningKeyNotFoundException( "No key found in " + uri.toString() + " with id " + keyId, null );
    }

    private List<Jwk> getAll() throws SigningKeyNotFoundException {
        try {
            ResponseEntity<String> response = Tools.restTemplate(3000,3000).getForEntity( uri, String.class );
            final List<Map<String, Object>> keys = ( List<Map<String, Object>> ) new ObjectMapper().readValue( response.getBody(), Map.class ).get( "keys" );
            if( keys == null || keys.isEmpty() )
                throw new SigningKeyNotFoundException( "No keys found in " + uri, null );

            List<Jwk> jwks = new ArrayList<>();
            for( Map<String, Object> values : keys ) {
                jwks.add( Jwk.fromValues( values ) );
            }
            return jwks;

        } catch( RestClientException ex ) {
            throw new SigningKeyNotFoundException( "Failed to connect " + uri, ex );
        } catch( IOException ex ) {
            throw new SigningKeyNotFoundException( "Failed to parse jwk", ex );
        }
    }
}