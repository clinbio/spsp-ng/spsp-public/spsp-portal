package swiss.sib.clinbio.spspportal.keycloack;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.http.HttpHeaders;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.security.interfaces.RSAPublicKey;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class KeycloakJwtToken {

    private final KeycloakJwkProvider jwkProvider;
    static final String BEARER = "Bearer ";
    private Jwk jwk = null;
    private LocalDateTime jwkTime;

    KeycloakJwtToken( String url ) {
        jwkProvider = new KeycloakJwkProvider( url );
    }

    public DecodedJWT decodeToken( HttpHeaders headers ) throws InvalidTokenException {
        return decodeToken( getAuthorizationToken( headers ) );
    }

    String getAuthorizationToken( HttpHeaders headers ) throws InvalidTokenException {
        List<String> authorizationList = headers.get( "Authorization" );
        if( authorizationList == null || authorizationList.isEmpty() )
            throw new InvalidTokenException( "Authorization not found" );
        String authorization = authorizationList.get( 0 );
        if( !authorization.startsWith( BEARER ) )
            throw new InvalidTokenException( "Authorization Bearer not found" );

        return authorization.substring( BEARER.length() );
    }

    DecodedJWT decodeToken( String value ) throws InvalidTokenException {
        if( value == null )
            throw new InvalidTokenException( "Token has not been provided" );

        DecodedJWT decodedJWT = JWT.decode( value );
        if( !decodedJWT.getType().equals( "JWT" ) )
            throw new InvalidTokenException( "Token is not JWT type" );

        Date expiryDate = decodedJWT.getExpiresAt();
        if( expiryDate != null && expiryDate.before( new Date() ) )
            throw new InvalidTokenException( "Token is expired " + Tools.timeToString( expiryDate )
                    + " for " + decodedJWT.getClaim( AccessToken.CLAIM_NAME ).asString() );

        LocalDateTime now = LocalDateTime.now();
        try {
            if( jwk == null || jwkTime.until( now, ChronoUnit.HOURS ) > 24 ) {
                jwk = jwkProvider.get( decodedJWT.getKeyId() );
                jwkTime = now;
                if( jwk == null )
                    throw new InvalidTokenException( "Jwk provider" );
            }

            Algorithm algorithm = Algorithm.RSA256( ( RSAPublicKey ) jwk.getPublicKey(), null );
            algorithm.verify( decodedJWT );

        } catch( JwkException | SignatureVerificationException ex ) {
            throw new InvalidTokenException( "Token has invalid signature " + ex );
        }

        return decodedJWT;
    }


    List<String> getRoles( String resourceName, DecodedJWT decodedJWT ) throws KeycloakJwtToken.InvalidTokenException {
        JsonNode node = null;
        try {
            node = decodedJWT.getClaim( "resource_access" ).as( JsonNode.class );
            node = node.get( resourceName ).get( "roles" );
            List<String> rolesList = new ArrayList<>();
            for( int i = 0; i < node.size(); i++ )
                rolesList.add( node.get( i ).asText() );
            return rolesList;

        } catch( Exception ex ) {
            throw new KeycloakJwtToken.InvalidTokenException( "Resource roles not found: " + node );
        }
    }

    List<String> getGroups( DecodedJWT decodedJWT ) throws KeycloakJwtToken.InvalidTokenException {
        JsonNode node = null;
        try {
            node = decodedJWT.getClaim( "groups" ).as( JsonNode.class );
            List<String> groupList = new ArrayList<>();
            for( int i = 0; i < node.size(); i++ )
                groupList.add( node.get( i ).asText() );
            return groupList;

        } catch( Exception ex ) {
            throw new KeycloakJwtToken.InvalidTokenException( "Resource groups not found: " + node );
        }
    }

    public static class InvalidTokenException extends Exception {
        public InvalidTokenException( String message ) {
            super( message );
        }

        public InvalidTokenException( String message, Throwable cause ) {
            super( message, cause );
        }
    }

    String claimsToString( HttpHeaders headers ) throws InvalidTokenException {
        DecodedJWT decodedJWT = decodeToken( getAuthorizationToken( headers ) );
        Map<String, Claim> claims = decodedJWT.getClaims();
        StringJoiner joiner = new StringJoiner( "\n" );
        for( Map.Entry<String, Claim> entry : claims.entrySet() ) {
            joiner.add( entry.getKey() );
            joiner.add( entry.getValue().as( JsonNode.class ).asText() );
        }
        return joiner.toString();
    }
}
