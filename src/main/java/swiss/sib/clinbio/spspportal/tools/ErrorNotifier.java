package swiss.sib.clinbio.spspportal.tools;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestClientException;
import swiss.sib.clinbio.spspshared.tools.Tools;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
public class ErrorNotifier extends AppenderBase<ILoggingEvent> {
    private static final long MIN_TIME_BETWEEN_EMAIL_IN_SECONDS = 30;
    private StringJoiner report = new StringJoiner( "\n" );
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> scheduledTask = null;

    @Override
    protected void append( ILoggingEvent iLoggingEvent ) {
        if( iLoggingEvent.getLevel() != Level.ERROR )
            return;
        report.add( Tools.timeToString( LocalDateTime.now() ) + " " +
                iLoggingEvent.getFormattedMessage() + " from " + iLoggingEvent.getLoggerName() );

        if( scheduledTask != null )
            scheduledTask.cancel( false );
        scheduledTask = scheduler.schedule( this::run, MIN_TIME_BETWEEN_EMAIL_IN_SECONDS, TimeUnit.SECONDS );
    }

    public void run() {
        try {
            Map<String, Object> params = new HashMap<>();
            params.put( "body", report.toString() );
            Tools.restTemplate().postForEntity(
                    SpspProperties.get().getServerUrl() + "/portal_error",
                    params, String.class );
        } catch( RestClientException ex ) {
            log.error( "Notify error: " + ex.getMessage() );
        }
        report = new StringJoiner( "\n" );
    }
}
