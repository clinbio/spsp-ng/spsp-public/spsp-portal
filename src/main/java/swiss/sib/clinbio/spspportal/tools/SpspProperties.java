package swiss.sib.clinbio.spspportal.tools;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import swiss.sib.clinbio.spspshared.tools.SharedProperties;

@Configuration
@ConfigurationProperties( "spsp" )
@Getter
@Setter
public class SpspProperties {

    @Value( "${server.port}" )
    private String apiPort;
    private String urlServer;
    private String urlLoader;
    private String urlKeycloak;

    public static SpspProperties get() {
        return properties;
    }

    static private SpspProperties properties;

    @Autowired
    public SpspProperties( Environment environment ) {
        properties = this;

        if( !SharedProperties.isRelease( environment ) )
            throw new RuntimeException( "BAD ENVIRONMENTS: not 'release'" );
    }

    public boolean isServerRunning() {
        return SharedProperties.checkRunning( getServerUrl() + "/isRunning" );
    }

    public boolean isLoaderRunning() {
        return SharedProperties.checkRunning( getLoaderUrl() + "/isRunning" );
    }

    public boolean isSpspRunning() {
        return isServerRunning() && isLoaderRunning();
    }

    public boolean isSpspWorking() {
        return SharedProperties.checkRunning( getServerUrl() + "/isRunningTask" ) &&
                SharedProperties.checkRunning( getLoaderUrl() + "/isRunningTask" );
    }

    public String getServerUrl() {
        return urlServer;
    }

    public String getLoaderUrl() {
        return urlLoader;
    }

}